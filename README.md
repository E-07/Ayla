<p align="center"><img src="https://cdn.discordapp.com/avatars/746555561948217356/e4af2e183de01bcc722096e2a1675064.png?size=1024" alt="Gray shape shifter" height="150"/></p>
<h1 align="center">Ayla</h1>


<p align="center">
<img alt="GitHub Repo stars" src="https://img.shields.io/github/stars/Ayla-Bot/Ayla?style=flat-square" height="25">
<br>

<img alt="Discord" src="https://img.shields.io/discord/739290644928921740?label=Suporte&logoColor=ffb00ff&style=flat-square">
</p>
<br>


👋 Olá, tudo bom? Eu me chamo **Ayla** Sou um bot para o Discord, Meus comandos são focados em diversão e moderação! Minha equipe está sempre ativa ou adicionado mais comando ou corringindo bugs!

Você quer reportar algum **bug**? Ou simplesmente fazer uma sugestão? Então abra uma **issue** e espere a minha equipe responder!

-----
- Links
   - [Me Adicione!](https://discord.com/oauth2/authorize?client_id=746555561948217356&scope=bot&permissions=8)
   - [Servidor de Suporte](https://discord.gg/Yna6wKA)

<h2 align="center">👑 Minha equipe</h2>

Se não fosse a minha equipe eu nunca existiria!
<br>

Equipe | Função
--------- | ------
07#0607 | Dono e Desenvolvedor
вαyʑi_σƒc#3660 | Desenvolvedor
Fearless ツ#4730 | Suporte
Ninguem por aqui...😥 | Designer

Caso queira ajuda nessa minha pequena equipe da Ayla basta falar com alguns dos donos.

<h2 align="center">✌ Parceiros oficiais </h2>

> Mais, o que são parceiros oficiais?

**Parceiros oficiais** são servidores que ajuda a Ayla toda hora que ela precisar! Ele são muitos **feras**!

Servidor | Dono do Servidor | insígnias
--------|-------|------
[The Highest](https://discord.gg/NsZJKBp) | VK | <img alt="Discord" src="https://img.shields.io/discord/705239991999135846?color=ff0000&label=The%20Highest">

**Muito obrigado mesmo!** <img src="https://cdn.discordapp.com/emojis/609284770743975946.gif" height=12 >
<h2 align="center">📃 Licença </h2>

Esse repositório está protegido pela licença **MIT** mais informações no arquivo **LICENSE**
