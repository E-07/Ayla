const axios = require('axios');

async function getImage() {
	let url = 'https://api.thecatapi.com/v1/images/search/';

	var res = await axios.get(url);

	return res;
}

module.exports = {
	getImage
};
