const Discord = require('discord.js');
const { getImage } = require('../../apis/cat.js');

module.exports = {
	name: 'cat',
	category: 'animais',
	bug: true,
	aliases: ['gato', 'gatinho', 'gata'],
	run: async (client, message, args) => {
		let link = await getImage();
		link.data.forEach(a => {
			let embed = new Discord.MessageEmbed()
				.setTitle(`🐱 aiin que fofo :3`)
				.setImage(a.url)
				.setColor('#fb00ff');

			message.channel.send(embed);
		});
	}
};
