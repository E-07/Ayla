const Discord = require('discord.js');
const { getImage } = require('../../apis/dog.js');

module.exports = {
	name: 'dog',
	category: 'animais',
	aliases: ['cachorro', 'doguinho', 'cachorrinho'],
	run: async (client, message, args) => {
		let link = await getImage();

		link.data.forEach(a => {
			let embed = new Discord.MessageEmbed()
				.setTitle(`:dog: Doguinho`)
				.setImage(a.url)
				.setColor('#fb00ff');

			return message.channel.send(embed);
		});
	}
};
