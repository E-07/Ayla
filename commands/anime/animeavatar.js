const Discord = require('discord.js');
const client = require('nekos.life');
const { sfw } = new client();

module.exports = {
	name: 'animeavatar',
	aliases: ['avataranime', 'aa'],
	category: 'Anime',
	run: (client, message) => {
		sfw.avatar().then(neko => {
			const attachment = new Discord.MessageAttachment(neko.url, 'avatar.png');
			message.channel.send(attachment);
		});
	}
};
