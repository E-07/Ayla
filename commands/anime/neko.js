const Discord = require('discord.js');
const client = require('nekos.life');
const { sfw } = new client();

module.exports = {
	name: 'neko',
	category: 'Anime',
	run: (client, message) => {
		sfw.neko().then(neko => {
			const attachment = new Discord.MessageAttachment(neko.url, 'neko.png');
			message.channel.send(attachment);
		});
	}
};
