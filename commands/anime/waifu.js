const Discord = require('discord.js');
const client = require('nekos.life');
const { sfw } = new client();

module.exports = {
	name: 'waifu',
	category: 'Anime',
	run: (client, message) => {
		sfw.waifu().then(neko => {
			const attachment = new Discord.MessageAttachment(neko.url, 'waifu.png');
			message.channel.send(attachment);
		});
	}
};
