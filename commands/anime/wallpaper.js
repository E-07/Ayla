const Discord = require('discord.js');
const client = require('nekos.life');
const { sfw } = new client();

module.exports = {
	name: 'wallpaper',
	category: 'Anime',
	run: (client, message) => {
		sfw.wallpaper().then(neko => {
			const attachment = new Discord.MessageAttachment(
				neko.url,
				'wallpaper.png'
			);
			message.channel.send(attachment);
		});
	}
};
