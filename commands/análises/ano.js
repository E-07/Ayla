module.exports = {
	name: 'ano',
	category: 'análises',

	aliases: ['year'],
	run: (client, message, args) => {
		const anoc = new Date().getFullYear();

		if (!args[0]) {
			return message.reply('Por favor, escreva um número depois do comando!');
		}

		if (isNaN(args[0])) {
			return message.reply('Por favor, escrevar um número depois do comando!');
		}
		if (args[0] >= anoc || args[0] === '0') {
			return message.channel.send('Essa pessoa não nasceu ainda :/');
		}

		let ano = args[0];

		message.reply(
			`Se uma pessoa tem **${ano}** ano(s) ela nasceu no ano **${anoc - ano}**.`
		);
	}
};
