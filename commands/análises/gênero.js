const axios = require('axios');

module.exports = {
	name: 'gender',
	category: 'análises',
	aliases: ['genero', 'gênero'],
	args: 'text',
	run: (client, message, args) => {
		let g = {
			female: '**feminino**',
			male: '**masculino**'
		};
		let nome = args.join(' ');
		const gender = axios
			.get('https://api.genderize.io/?name=' + nome)
			.then(a => {
				try {
					if (!a.data.gender)
						return message.reply(
							`Não tenho ideia do gênero do nome **${a.data.name}** `
						);
					if (
						nome.toLowerCase() === 'feminino' ||
						nome.toLowerCase() === 'masculino' ||
						nome.toLowerCase() === 'a' ||
						nome.toLowerCase() === 'o'
					) {
						return message.reply(':/');
					}
					return message.reply(
						`Eu tenho **${Math.round(
							a.data.probability * 100
						)}%** de certeza quer o nome **${nome}** é ${g[a.data.gender]}
				`
					);
				} catch (err) {
					return message.reply(
						`Oh não, aconteceu um error ao executar esse comando: ${err}`
					);
				}
			})
			.catch(a => {});
	}
};
