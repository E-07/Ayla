const Discord = require('discord.js');
module.exports = {
	name: 'botinfo',
	category: "bot",
	aliases: ['infobot', 'info'],
	run: (client, message, args) => {
		function rn(min, max) {
			return parseInt(Math.random() * (max - min) + min);
		}

		let bicon = message.client.user.displayAvatarURL();
		let bnome = message.client.user.username;
		let dono = client.users.cache.get('532799065143115777');
		var info = new Discord.MessageEmbed()
			.setThumbnail(bicon)
			.setTitle(`Essas são minhas informações!`)
			.setDescription(
				`
			
👋 Olá, tudo bem? eu me chamo Ayla, sou um bot focado em diversão e moderação!! 

			
Eu estou em **${
					client.guilds.cache.size
				} Servidores**, e estou levando alegria para **${
					client.users.cache.size
				} Usuários! 🌷**
			
Fui criada utilizando a biblioteca **Discord.js** <:emoji_60:757079001369149490> Linguagem de programação é **JavaScript** <:js:740786327657906187> usando **Node.js** <:nodejs:740786623838421304>
		
Caso queira reportar um bug 🐛 ou sugerir um comando bastar [Clicar Aqui](https://discord.gg/Yna6wKA)
			
Se você quiser você pode me adicionar no seu **servidor** [Clicando aqui!](https://discord.com/oauth2/authorize?client_id=746555561948217356&scope=bot&permissions=8) <a:emoji_56:757075329050476564>`
			)
			.addField('<a:emoji_57:757075377989484584> Versão da Ayla', '1.8.4')
			.addField('<:nodejs:740786623838421304> Node.js', 'v12.16.1')
			.addField('<:emoji_60:757079001369149490> Discord.js', 'v12')
			.addField('<:emoji_8:740786495383928873> Memória', `50 MB`)
			.addField('<:emoji_12:740786575658582086> Memória RAM', '8 GB')
			.setFooter('Ayla foi criada pelo 07#0607', dono.avatarURL())

			.setTimestamp()
			.setColor('#fb00ff');
		message.channel.send(info);
	}
};
