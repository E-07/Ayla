const { MessageEmbed } = require('discord.js');

module.exports = {
	name: 'help',
	category: 'bot',
	aliases: ['ajuda', 'comandos', 'commands', 'cmd', 'cmds'],
	run: async (client, message, args) => {
		if (args[0]) {
			const command = await client.commands.get(args[0]);

			if (!command) {
				return message.channel.send('Comando desconhecido ' + args[0]);
			}

			let embed = new MessageEmbed()
				.setAuthor(command.name, client.user.displayAvatarURL())
				.addField('Description', command.description || 'Não providenciada')
				.addField('Usage', '`' + command.usage + '`' || 'Não providenciada')
				.setThumbnail(client.user.displayAvatarURL())
				.setColor('fb00ff')
				.setFooter(client.user.username, client.user.displayAvatarURL());

			return message.channel.send(embed);
		} else {
			const commands = await client.commands;

			let emx = new MessageEmbed()
				.setColor('fb00ff')
				.setFooter(client.user.username, client.user.displayAvatarURL())
				.setThumbnail(client.user.displayAvatarURL());

			let com = {};
			for (let comm of commands.array()) {
				let category = comm.category || 'Desconhecida';
				let name = comm.name;
                if(comm.dono || comm.esc === true) continue
				if (!com[category]) {
					com[category] = [];
				}
				com[category].push(name);
			}

			for (const [key, value] of Object.entries(com)) {
				let category = key;

				let desc = '`' + value.join('`, `') + '`';

				emx.addField(`${category.toUpperCase()} [${value.length}]`, desc);
			}

			return message.channel.send(emx);
		}
	}
};
