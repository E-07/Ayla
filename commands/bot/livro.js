const { MessageEmbed } = require('discord.js');
module.exports = {
	name: 'livro',
	aliases: ['recordaçoes', 'recordacoes', 'recordacões', 'recordações'],
	category: 'bot',
	run: (client, message, args) => {
		let pages = ['**Olá, seja bem vindo(a) a minhas recordações!**\n\nEsse lugar tem tudo que ja aconteceu comigo até hoje.', '**07/07/2020 - Eu fui criada!**\n\n**21/08/2020 - **Meu token foi roubado, e sair de 101 servidores** \n\n**21/08/2020 - Fui criada novamente!**\n\n**30/08/2020 - O querido Chat Global foi criado!**\n\n**01/09/2020 - Primeiros Comandos de Economia!**\n\n**23/09/2020 - Cheguei a 100 servidores!**\n\n**29/09/2020 - Fui Verificada!', 'fim...'];
		let page = 1;

		const embed = new MessageEmbed()
			.setColor('fb00ff')
			.setFooter(`Página ${page} de ${pages.length}`)
			.setDescription(pages[page - 1]);

		message.channel.send(embed).then(msg => {
			msg.react('⏪').then(r => {
				msg.react('⏩');

				const backwardsFilter = (reaction, user) =>
					reaction.emoji.name === '⏪' && user.id === message.author.id;
				const forwardsFilter = (reaction, user) =>
					reaction.emoji.name === '⏩' && user.id === message.author.id;

				const backwards = msg.createReactionCollector(backwardsFilter, {
					time: 60000
				});
				const forwards = msg.createReactionCollector(forwardsFilter, {
					time: 60000
				});

				backwards.on('collect', r => {
					if (page === 1)
						return r.users.remove(message.author.id).catch(function() {});
					page--;
					embed.setDescription(pages[page - 1]);
					embed.setFooter(`Página ${page} de ${pages.length}`);
					msg.edit(embed);
					r.users.remove(message.author.id).catch(function() {});
				});

				forwards.on('collect', r => {
					if (page === pages.length)
						return r.users.remove(message.author.id).catch(function() {});
					page++;
					embed.setDescription(pages[page - 1]);
					embed.setFooter(`Página ${page} de ${pages.length}`);
					msg.edit(embed);
					r.users.remove(message.author.id).catch(function() {});
				});
			});
		});
	}
};
