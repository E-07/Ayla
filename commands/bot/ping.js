module.exports = {
	name: 'ping',
	category: "bot",
	aliases: [],
	run: async (client, message, args) => {
		const m = await message.channel.send(
			'<a:emoji_5:740786441055109170> | **Processando...**'
		);
		try {
			m.edit(
				`🏓 **| Pong!**\nMinha latência: **${m.createdTimestamp -
					message.createdTimestamp}ms.**\nWebSocket: **${Math.round(
					client.ws.ping
				)}ms**`
			);
		} catch (e) {
			m.edit(
				'<:emoji_14:740786601722118166> | Ocorreu um **Error** ao tentar executar esse comando por favor tente mais tarde!'
			);
		}
	}
};
