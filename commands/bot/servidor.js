const Discord = require('discord.js');

module.exports = {
	name: 'servers',
	category: 'bot',
	aliases: ['servidor', 'servidores'],
	run: (client, message, args) => {
	    
		let embed = new Discord.MessageEmbed()
			.setTitle('Servidores')
			.setDescription(
				`Atualmente eu estou levando alegria 😘 para **${
					client.guilds.cache.size
				}** Servidores!`
			)
			.setColor('fb00ff');
			
		message.channel.send(embed);
	}
};
