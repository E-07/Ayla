const Discord = require('discord.js');
let days = 0;
let week = 0;

module.exports = {
	name: 'uptime',
	category: "bot",
	aliases: ['up'],
	run: (client, message, args) => {
		let uptime = ``;
		let totalSeconds = client.uptime / 1000;
		let hours = Math.floor(totalSeconds / 3600);
		totalSeconds %= 3600;
		let minutes = Math.floor(totalSeconds / 60);
		let seconds = Math.floor(totalSeconds % 60);

		if (hours > 23) {
			days = days + 1;
			hours = 0;
		}

		if (days == 7) {
			days = 0;
			week = week + 1;
		}

		if (week > 0) {
			uptime += `${week} Semanas, `;
		}

		if (minutes > 60) {
			minutes = 0;
		}

		uptime += `Estou Acordada à: ${days} dias, ${hours} horas, ${minutes} minutos e ${seconds} segundos`;

		let embed = new Discord.MessageEmbed()
			.setColor('#fb00ff')
			.addField('Tempo Acordada', uptime);
		message.channel.send(embed);
	}
};
