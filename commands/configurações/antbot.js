module.exports = {
	name: 'antbot',
	aliases: ['antibot'],
	category: 'configuração',
	description: 'Ativa ou Desativa o anti bot!',
	usage: 'antibot',
	run: (client, message, args, db) => {
		if (message.author.id !== message.guild.ownerID) {
			return message.reply(
				'**Para total segurança infelizmente só o dono do servidor pode usar esse comando**'
			);
		}
		db.ref(`servers/${message.guild.id}`)
			.once('value')
			.then(r => {
				if (r.val().bot === false) {
					db.ref(`servers/${message.guild.id}`).update({
						bot: true
					});

					message.channel.send('**Pronto!, agora o anti bot está ativo!**');
				} else if (r.val().bot === true) {
					db.ref(`servers/${message.guild.id}`).update({
						bot: false
					});
					message.channel.send(
						'**Pronto!, agora o anti bot está desativado!**'
					);
				} else {
					db.ref(`servers/${message.guild.id}`).set({
						nome: message.guild.name,
						id: message.guild.id,
						prefixo: 'a!',
						dono: message.guild.ownerID,
						bot: false,
						invite: false
					});
					message.channel.send(
						'**Ocorreu erro há tentar executar esse comando! Por favor tente denovo!**'
					);
				}
			});
	}
};
