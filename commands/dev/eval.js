const Discord = require('discord.js');

module.exports = {
	name: 'eval',
	dono: true,
	category: 'donos',
	run: (client, message, args) => {
		try {
			var code = args.join(' ');
			var evaled = eval(code);
			if (code.includes('process.env') || code.includes('token')) {
				return message.reply('Desculpa mas não vou executar isso...');
			}
			if (typeof evaled !== 'string') evaled = require('util').inspect(evaled);

			const embed = new Discord.MessageEmbed()
				.setColor('#fb00ff')
				.addField(':inbox_tray: Entrada: ', `\`\`\`${code}\`\`\``)
				.addField(
					':outbox_tray: Saída: ',
					`\`\`\`js\n${clean(evaled)}\n\`\`\``
				);
			message.channel.send(embed);
		} catch (err) {
			const embed = new Discord.MessageEmbed()
				.setColor('#fb00ff')
				.addField(':inbox_tray: Entrada: ', `\`\`\`${code}\`\`\``)
				.addField(':outbox_tray: Saída: ', `\`\`\`${clean(err)}\`\`\``);
			message.channel.send(embed);
		}

		function clean(text) {
			if (typeof text === 'string')
				return text
					.replace(/`/g, '`' + String.fromCharCode(8203))
					.replace(/@/g, '@' + String.fromCharCode(8203));
			else return text;
		}
	}
};
