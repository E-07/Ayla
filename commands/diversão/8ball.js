const Discord = require('discord.js');
module.exports = {
	name: '8ball',
	category: "diversão",
	aliases: ['ball', 'simnao'],
	args: 'text',
	run: (client, message, args) => {
		var a = [
			'sim!',
			'não',
			'hummmm...',
			'Só você sabe',
			'provavelmente que não',
			'provavelmente que sim!',
			'talvez...',
			'Acho que não...',
			'Acho que sim!'
		];

		var r = Math.floor(Math.random() * a.length);

		message.channel.send(a[r]);
	}
};
