const discord = require('discord.js');
const figlet = require('figlet');

module.exports = {
	name: 'ascii',
	category: "diversão",
	args: "text",
	run: (client, message, args) => {
		let text = args.join(' ');
		let maxlen = 20;
		if (text.length > 20) {
			return message.channel.send(`Por favor forneça até 20 caracteres!`);
		}

		figlet(text, function(err, data) {
			message.channel.send(data, {
				code: 'AsciiArt'
			});
		});
	}
};
