const { stripIndent } = require('common-tags');

module.exports = {
	name: 'charlie-charlie',
	args: 'text',
	aliases: ['cc'],
	category: "diversão",
	run: (client, message, args) => {
		let answers = ['yes', 'no'];
		const answer = answers[Math.floor(Math.random() * answers.length)];
		return message.channel.send(stripIndent`
			${args.join(' ')}
			\`\`\`
			    ${answer === 'no' ? '\\' : ' '}  |  ${answer === 'yes' ? '/' : ' '}
			  NO ${answer === 'no' ? '\\' : ' '} | ${answer === 'yes' ? '/' : ' '}YES
			      ${answer === 'no' ? '\\' : ' '}|${answer === 'yes' ? '/' : ' '}
			————————————————
			      ${answer === 'yes' ? '/' : ' '}|${answer === 'no' ? '\\' : ' '}
			  YES${answer === 'yes' ? '/' : ' '} | ${answer === 'no' ? '\\' : ' '}NO
			    ${answer === 'yes' ? '/' : ' '}  |  ${answer === 'no' ? '\\' : ' '}
			\`\`\`
		`);
	}
};
