const Discord = require('discord.js');

module.exports = {
	name: 'coinflip',
	category: "diversão",
	aliases: ['caraoucora', 'moeda', 'caracoroa'],
	run: (client, message, args) => {
		var array1 = ['cara', 'coroa'];

		var rand = Math.floor(Math.random() * array1.length);

		if (
			!args[0] ||
			(args[0].toLowerCase() !== 'cara' && args[0].toLowerCase() !== 'coroa')
		) {
			message.reply('insira **cara** ou **coroa** depois do comando.');
		} else if (args[0].toLowerCase() == array1[rand]) {
			message.channel.send(
				'A moeda caiu em **' + array1[rand] + '**, você ganhou!'
			);
		} else if (args[0].toLowerCase() != array1[rand]) {
			message.channel.send(
				'A moeda caiu em **' + array1[rand] + '**, você perdeu!'
			);
		}
	}
};
