const Discord = require('discord.js');

module.exports = {
	name: 'escolher',
	args: 'text',
	aliases: ['choice', 'escolhe'],
	category: 'diversão',
	run: (client, message, args) => {
		if (!args.includes('|')) {
			return message.reply('Por favor, coloque ``|`` para separa as escolhas!');
		}

		let choose = message.content.substring(8).split('|');
		let choice = choose[Math.floor(Math.random() * choose.length)];
		let msg = `**Eu escolho:** \`${choice}\``;

		if (choose.length < 2) {
			message.channel.send('**Eu preciso de duas opções para escolher!**');
		} else {
			message.channel.send(msg);
		}
	}
};
