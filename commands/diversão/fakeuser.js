module.exports = {
	name: 'fakeuser',
	category: 'diversão',
	aliases: ['userfake'],
	run: async (client, message, args) => {
		let user =
			client.users.cache.get(args[0]) ||
			message.mentions.users.first() ||
			client.users.cache.find(a => a.username === args[0]);
		if (!user) {
			return message.reply('Por favor, Mencione um usuário depois do comando!');
		}
		if (!args[1]) {
			return message.reply(
				'Por favor, escreva o que o usuário vai dizer depois do comando! exemplo: fakeuser ``@user` ``<mensagem>``'
			);
		}
		message.delete();
		await message.channel
			.createWebhook(user.username, {
				avatar: user.avatarURL()
			})
			.then(async a => {
				await a.send(args.slice(1).join(' '));
				a.delete();
			})
			.catch(console.error);
	}
};
