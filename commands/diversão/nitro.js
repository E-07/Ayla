const Discord = require('discord.js');

module.exports = {
	name: 'nitro',
	category: 'diversão',
	aliases: ['fakenitro', 'nitrofake'],
	run: (client, message, args) => {
		const attachment = new Discord.MessageAttachment(
			'https://i.imgur.com/aRTbfYQ.png',
			'nitro.png'
		);
		message.delete();
		message.channel.send(attachment);
	}
};
