const Discord = require('discord.js');

module.exports = {
	name: 'reverse',
	category: "diversão",
	aliases: ['reverso', 'rv'],
	args: 'text',
	run: (client, message, args) => {
		const str = args.join(' ');
		let msg = message.reply(
			str.split('').reverse().join('')
		);
	}
};
