const Discord = require('discord.js');

module.exports = {
	name: 'say',
	category: "diversão",
	aliases: ['falar'],
	args: 'text',
	run: (client, message, args) => {
		const say = args.join(' ');
		message.delete().catch(a => {});
		message.channel.send(say);
	}
};
