const Discord = require('discord.js');

module.exports = {
	name: 'snipe',
	category: "diversão",
	aliases: ['sp'],
	run: (client, message, args) => {
		const msg = client.snipes.get(message.channel.id);
		if (!msg)
			return message.reply(
				'Ihh, não encontrei nem uma mensagem deletada nesse canal!'
			);

		const embed = new Discord.MessageEmbed()
			.setAuthor(`${msg.author} Falou:`)
			.setColor('fb00ff')
			.setDescription(msg.content);

		if (msg.image) embed.setImage(msg.image);

		message.channel.send(embed).catch(a => {});
	}
};
