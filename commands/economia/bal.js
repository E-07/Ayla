module.exports = {
	name: 'bal',
	category: 'economia',
	aliases: ['atm', 'flores', 'flor', 'balance'],
	run: (client, message, args, db) => {
		try {
			let m =
				message.mentions.users.first() ||
				client.users.cache.get(args[0]) ||
				client.users.cache.find(user => user.username === args[0]) ||
				message.author;
	
			if (m) {
				if (m.id === message.author.id) {
					db.ref(`users/${m.id}`)
						.once('value')
						.then(a => {
							if (a.val() === null) {
								message.reply(
									'Você não tem uma conta criada no meu sistema! use **criar**'
								);
							} else {
								message.reply(`Você possuí **${a.val().flores}** flores!`);
							}
						});
				} else {
					db.ref(`users/${m.id}`)
						.once('value')
						.then(a => {
							if (a.val() === null) {
								message.reply(
									'O usuário não tem uma conta criada no meu sistema!'
								);
							} else {
								message.reply(
									`O Usuário **${m.username}** possuí **${
										a.val().flores
									}** flores!`
								);
							}
						});
				}
			} else {
				db.ref(`users/${message.author.id}`)
					.once('value')
					.then(a => {
						if (a.val() === null) {
							message.reply(
								'Você não tem uma conta criada no meu sistema! use **criar**'
							);
						} else {
							message.reply(`Você possuí **${a.val().flores}** Flores!`);
						}
					});
			}
		} catch (e) {}
	}
};
