module.exports = {
	name: 'criar',
	category: "economia",
	run: (client, message, args, db) => {
		try {
			db.ref(`users/${message.author.id}`)
				.once('value')
				.then(a => {
					if (a.val()) {
						message.reply('Você já possui uma conta no meu sistema!');
					} else {
						try {
							db.ref(`users/${message.author.id}`).set({
								nome: message.author.username,
								id: message.author.id,
								flores: 0
							});
						} catch (e) {
							message.reply(`Ocorreu um error: \`\`${e}\`\``);
						}
						message.reply('Sua conta foi criada com sucesso!');
					}
				});
		} catch (e) {}
	}
};
