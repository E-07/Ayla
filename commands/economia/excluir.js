module.exports = {
	name: 'excluir',
	category: 'economia',
	aliases: ['apagar'],
	run: (client, message, args, db) => {
		try {
			db.ref(`users/${message.author.id}`)
				.once('value')
				.then(a => {
					if (a.val() === null) {
						message.reply(
							'Você não tem uma conta no meu sistema! então não tem como apagar... 😅'
						);
					} else {
						try {
							message
								.reply(
									'Por favor, reaja na reação ❌ para excluir sua conta!'
								)
								.then(msg => {
									msg.react('❌');
									const byeF = (reaction, user) =>
										reaction.emoji.name === '❌' &&
										user.id === message.author.id;
									const bye = msg.createReactionCollector(byeF, {
										time: 60000
									});
									bye.on('collect', r => {
										db.ref(`users/${message.author.id}`).remove();
										msg.edit('Sua conta foi excluída com sucesso!');
									});
								});
						} catch (e) {
							message.reply(
								'Essa não... parece que ocorreu um error, esses bugs...🙄 ``' +
									e +
									'``'
							);
						}
					}
				});
		} catch (e) {}
	}
};
