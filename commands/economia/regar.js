const talkedRecently = new Set();

module.exports = {
	name: 'regar',
	category: 'economia',
	aliases: ['rg', 'work', 'trabalhar'],
	run: (client, message, args, db) => {
		if (talkedRecently.has(message.author.id)) {
			message.channel.send('🍂').then(msg => {
				setTimeout(function() {
					msg.edit('🍂🥀');
				}, 1000);
				setTimeout(function() {
					msg.edit(
						`<@${message.author.id}>` +
							' Por favor espere 5 minuto! para regar suas flores novamente! ou elas vão morrer...'
					);
				}, 3000);
			});
		} else {
			try {
				db.ref(`users/${message.author.id}`)
					.once('value')
					.then(r => {
						if (r.val() === null) {
							message.channel.send(
								'Você não tem uma conta em meu sistema! por favor escrevar **criar**'
							);
						}
						let b = Math.round(Math.random() * (100 - 10) + 10);

						let flores = Number(r.val().flores) + b;

						db.ref(`users/${message.author.id}`).update({
							nome: message.author.username,
							flores: flores
						});

						message.channel.send('🌷').then(msg => {
							setTimeout(function() {
								msg.edit('🌷🌹');
							}, 1000);
							setTimeout(function() {
								msg.edit('🌷🌹🌺');
							}, 2000);
							setTimeout(function() {
								msg.edit('🌷🌹🌺🌻');
							}, 3000);
							setTimeout(function() {
								msg.edit('🌷🌹🌺🌻🌼');
							}, 4000);
							setTimeout(function() {
								msg.edit(
									`<@${
										message.author.id
									}>, Parabéns você regou suas flores e elas se multiplicaram! Você ganhou mais** \`${b}\` **flores!`
								);
							}, 5000);
						});
					});
				talkedRecently.add(message.author.id);
				setTimeout(() => {
					talkedRecently.delete(message.author.id);
				}, 300000);
			} catch (e) {
				console.error(e);
			}
		}
	}
};
