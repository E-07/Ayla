const Discord = require('discord.js');

module.exports = {
	name: 'baby',
	category: 'imagem',
	aliases: ['bebe', 'bebê', 'bb', 'moana'],
	run: (client, message, args) => {
		let user =
			client.users.cache.get(args[0]) ||
			message.mentions.users.first() ||
			client.users.cache.find(a => a.username === args[0]) ||
			message.author;

		let img = message.attachments.first();

		let ver = img ? message.attachments.first().url : user.avatarURL();

		const attachment = new Discord.MessageAttachment(
			'https://useless-api.vierofernando.repl.co/baby?image=' + ver,
			'moana.png'
		);

		message.channel.send(attachment);
	}
};