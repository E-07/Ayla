const Discord = require('discord.js');

module.exports = {
	name: 'captcha',
	category: 'imagem',
	aliases: ['ct'],
	args: 'text',
	run: (client, message, args) => {
		let link = encodeURI(
			'https://api.alexflipnote.dev/captcha?text=' + args.join(' ')
		);
		let embed = new Discord.MessageEmbed()
			.setTitle('Captcha!')
			.setImage(link)
			.setColor('fb00ff');

		message.channel.send(embed);
	}
};
