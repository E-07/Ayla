const ayla = require('ayla-npm');
const Discord = require('discord.js');

module.exports = {
	name: 'captcha2',
	category: "imagem",
	aliases: ['ct2'],
	args: 'text',
	run: (client, message, args) => {
		let ct = ayla.captcha2({
			message: args
		});

		const attachment = new Discord.MessageAttachment(ct, 'captcha.png');

		message.channel.send(attachment);
	}
};
