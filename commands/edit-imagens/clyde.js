const { MessageEmbed } = require('discord.js');

module.exports = {
	name: 'clyde',
	category: "imagem",
	aliases: ['net-caiu'],
	args: 'text',
	run: (client, message, args, em) => {
		let clydeMessage = args.slice(0).join(' ');
		let encodedLink = encodeURI(
			`https://ctk-api.herokuapp.com/clyde/${clydeMessage}`
		);
		const clydeEmbed = new MessageEmbed()
			.setTitle('Eita aparece que a net caiu!')
			.setImage(encodedLink)
			.setColor('#fb00ff');

		message.channel.send(clydeEmbed);
	}
};
