const Discord = require('discord.js');
const fetch = require('node-fetch');

module.exports = {
	name: 'faketweet',
	aliases: ['tweet'],
	category: 'imagem',
	run: (client, message, args, bot) => {
		let user = args[0];

		let text = args.slice(1).join(' ') || undefined;

		if (!user)
			return message.reply(
				'Por favor, escrevar alguma coisa depois do comando!'
			);

		if (user.startsWith('@')) user = args[0].slice(1);
		const type =
			user.toLowerCase() === 'realdonaldtrump' ? 'trumptweet' : 'tweet';
		const u = user.startsWith('@') ? user.slice(1) : user;
		if (!text) return message.reply(ajuda);
		message.channel.startTyping();
		fetch(
			`https://nekobot.xyz/api/imagegen?type=${type}&username=${u}&text=${encodeURIComponent(
				text
			)}`
		)
			.then(res => res.json())
			.then(data => message.channel.send(em.setImage(data.message)))
			.catch(err => {});
		message.channel.stopTyping(true);
	}
};
