const Discord = require('discord.js');

module.exports = {
	name: 'ligando',
	category: 'imagem',
	aliases: ['fbi', 'loli', 'lolicon'],
	args: 'text',
	run: (client, message, args) => {
		let link = encodeURI(
			'https://api.alexflipnote.dev/calling?text=' + args.join(' ')
		);
		let embed = new Discord.MessageEmbed()
			.setTitle('FBI!!')
			.setImage(link)
			.setColor('BLACK');

		message.channel.send(embed);
	}
};
