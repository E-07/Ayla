const { qrcode } = require('ayla-npm');
const Discord = require('discord.js');

module.exports = {
	name: 'qrcode',
	args: 'text',
	category: "imagem",
	aliases: ['code', 'qr', 'codeqr', 'qr-code'],
	run: (client, message, args) => {
		let qr = qrcode({
			message: args
		});

		const attachment = new Discord.MessageAttachment(qr, 'qrcode.png');

		message.channel.send(attachment);
	}
};
