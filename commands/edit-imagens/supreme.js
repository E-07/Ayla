const Discord = require('discord.js');

module.exports = {
	name: 'supreme',
	args: 'text',
	category: "imagem",
	aliases: ['mansaosupreme', 'supremo'],
	run: (client, message, args) => {
		const link = `https://api.alexflipnote.dev/supreme?text=${encodeURIComponent(
			args.join(' ')
		)}`;

		let embed = new Discord.MessageEmbed()
			.setTitle('Supreme! 🔥')
			.setColor('fb00ff')
			.setImage(link);
		message.channel.send(embed).catch(a => {
			console.error(a);
		});
	}
};
