const Discord = require('discord.js');

module.exports = {
	name: 'xbox',
	category: "imagem",
	args: 'text',
	aliases: ['conquista2', 'emblemaxbox'],
	run: (client, message, args) => {
		const link = `http://www.achievement-maker.com/xbox/${encodeURIComponent(
			args.join(' ')
		)}?header=NOVA+CONQUISTA!&email=.png`;

		let embed = new Discord.MessageEmbed()
			.setTitle('Parabéns! sua conquista foi...')
			.setColor('fb00ff')
			.setImage(link);
		message.channel.send(embed);
	}
};
