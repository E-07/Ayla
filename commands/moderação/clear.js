const Discord = require('discord.js');

module.exports = {
	name: 'clear',
	category: 'administração',
	aliases: ['limpar'],
	run: async (client, message, args) => {
		if (!message.member.permissions.has('MANAGE_MESSAGES'))
			return message.reply(
				'Você precisa ter permissão de `Gerenciar Mensagens` para usar esse comando'
			);
		const deleteCount = parseInt(args[0], 10);
		if (!deleteCount || deleteCount < 1 || deleteCount > 99)
			return message.reply('Por favor, escrevar número de até **99** ');

		if (args[0] === isNaN) {
			message.reply('Isso não é um número!');
		}
		const fetched = await message.channel.messages.fetch({
			limit: deleteCount + 1
		});
		message.channel.bulkDelete(fetched);
		message.channel
			.send(`**Chat Limpo!**`)
			.then(msg => msg.delete({ timeout: 10000 }))
			.catch(error =>
				console.log(`Não foi possível deletar mensagens devido a: ${error}`)
			);
	}
};
