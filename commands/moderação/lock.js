const Discord = require('discord.js');

module.exports = {
	name: 'lock',
	category: 'administração',
	aliases: ['bloqueiar'],
	run: (client, message, args) => {
		if (!message.member.hasPermission('MANAGE_CHANNELS')) {
			return message.reply('**Você não tem permissão para usar esse comando**');
		}
		try {
			message.channel.updateOverwrite(message.channel.guild.roles.everyone, {
				SEND_MESSAGES: false
			});
		} catch (e) {}
		message.reply('Canal Bloqueado com Sucesso');
	}
};
