module.exports = {
	name: 'prefix',
	category: 'administração',
	aliases: ['setprefix', 'setarprefixo', 'prefixo'],
	run: (client, message, args, db) => {
		if (!message.member.hasPermission('MANAGE_MESSAGES')) {
			return message.channel.send(
				'Você não tem permissão para usar esse comando!'
			);
		}

		if (!args[0]) {
			return message.channel.send(
				'Por favor escreva o novo prefixo depois do comando!'
			);
		}

		if (args[1]) {
			return message.channel.send(
				'Você não pode setar o novo prefixo com dois argumentos!'
			);
		}

		if (args[0].length > 3) {
			return message.channel.send('O prefixo teve ser abaixo de 3 caracteres');
		}

		db.ref(`servers/${message.guild.id}`)
			.once('value')
			.then(a => {
				db.ref(`servers/${message.guild.id}`).update({
					prefixo: args[0]
				});

				message.channel.send(
					`<a:emoji_57:757075377989484584> | Agora meu prefixo nesse servidor é: **${
						args[0]
					}**`
				);
			});
	}
};
