const Discord = module.require('discord.js');

module.exports = {
	name: 'unlock',
	category: 'administração',
	run: (client, message, args) => {
		try {
			if (!message.member.hasPermission('MANAGE_CHANNELS')) {
				return message.channel.send(
					'Você não tem permissão para usa esse comando!'
				);
			}

			message.channel.updateOverwrite(message.channel.guild.roles.everyone, {
				SEND_MESSAGES: true
			});

			message.reply('Canal Desbloqueado com Sucesso!');
		} catch (e) {}
	}
};
