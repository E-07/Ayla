const weather = require('weather-js');
const Discord = require('discord.js');

module.exports = {
	name: 'clima',
	aliases: ['tempo'],
	category: "pesquisas",
	run: (client, message, args) => {
		weather.find(
			{
				search: args,
				degreeType: 'C'
			},
			function(err, result) {
				if (err) console.log(err);
				if (!result)
					return message.channel.send(
						'Escreva o nome da cidade depois do comando!'
					);
				if (!result[0])
					return message.channel.send(
						`😬 **»** Desculpe **${
							message.author.username
						}**, não encontrei essa cidade!`
					);
				const embed = new Discord.MessageEmbed()
					.setTitle(`**${result[0].location.name}**`)
					.addField(
						`**☀️ » Temperatura**`,
						`\`${result[0].current.temperature}°C\``,
						true
					)
					.addField(
						`**🌡️ » Sensação Térmica**`,
						`\`${result[0].current.feelslike}°C\``,
						true
					)
					.addField(`**💦 » Umidade**`, `\`${result[0].current.humidity}%\``)
					.addField(`**💨 » Vento**`, `\`${result[0].current.windspeed}\``)
					.setColor('fb00ff')

					.setThumbnail(result[0].current.imageUrl);

				message.channel.send(embed);
			}
		);
	}
};
