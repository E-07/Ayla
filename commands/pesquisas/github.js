const moment = require('moment');
const { MessageEmbed } = require('discord.js');
const request = require('node-superfetch');

module.exports = {
	name: 'github',
	category: 'pesquisas',
	run: async (client, message, args) => {
		let author = args[0];
		let repo = args[1];

		if (!author) {
			return message.reply(
				'Por favor, use o comando de forma certa: github ``<Autor> <Repositório>``'
			);
		} else if (!repo) {
			return message.reply(
				'Por favor, use o comando de forma certa: github ``<Autor> <Repositório>``'
			);
		}
		try {
			const { body } = await request.get(
				`https://api.github.com/repos/${author}/${repo}`
			);

			const embed = new MessageEmbed()
				.setColor(0xffffff)
				.setAuthor(
					'GitHub',
					'https://i.imgur.com/e4HunUm.png',
					'https://github.com/'
				)
				.setTitle(body.full_name)
				.setURL(body.html_url)
				.setDescription(body.description ? body.description : 'Sem Descrição.')
				.setThumbnail(body.owner.avatar_url)
				.addField('❯ Estrelas', Number(body.stargazers_count), true)
				.addField('❯ Forks', Number(body.forks), true)
				.addField('❯ Issues', Number(body.open_issues), true)
				.addField('❯ Linguagem ', body.language || 'Não sei...', true)
				.addField(
					'❯ Data de Criação',
					moment.utc(body.created_at).format('DD/MM/YYYY h:mm A'),
					true
				)
				.addField(
					'❯ Data da última Modificação',
					moment.utc(body.updated_at).format('DD/MM/YYYY h:mm A'),
					true
				);
			return message.channel.send(embed);
		} catch (err) {
			if (err.status === 404)
				return message.reply('Não achei nem um resultados.');
			return message.channel.send(`Error: \`${err.message}\``);
		}
	}
};
