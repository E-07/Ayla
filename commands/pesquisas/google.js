const Discord = require('discord.js');
const { google } = require('ayla-npm');

module.exports = {
	name: 'google',
	category: 'pesquisas',
	args: 'text',
	run: (client, message, args) => {
		let gg = google({
			message: args.join(' ')
		});

		const embed = new Discord.MessageEmbed()
			.setColor('F8F8F9')
			.setTitle('<:google:763803235231203419> Google')
			.setDescription(`
			[Clique Aqui!](${gg})`
			)
			.setFooter(`Comando solicitado por: ${message.author.username}`)
			.setThumbnail('https://i.imgur.com/ojL38iL.gif');

		message.channel.send(embed);
	}
};
