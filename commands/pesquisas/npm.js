const moment = require('moment');
const { MessageEmbed } = require('discord.js');
const axios = require('axios');

module.exports = {
	name: 'npm',
	aliases: ['package'],
	category: 'pesquisas',
	args: 'text',
	run: (client, message, args) => {
		let pkg = args.join(' ');

		axios
			.get(`https://registry.npmjs.com/${pkg}`)
			.then(body => {
				if (body.status === 404) {
					message.reply('Esse npm não existir!');
				}
				if (body.data.time.unpublished)
					return message.reply('Este pacote não é público!');
				const version = body.data.versions[body.data['dist-tags'].latest];
				const maintainers = Array(body.data.maintainers.map(user => user.name));
				const dependencies = version.dependencies
					? Array(Object.keys(version.dependencies))
					: null;
				const embed = new MessageEmbed()
					.setColor(0xcb0000)
					.setAuthor(
						'NPM',
						'https://i.imgur.com/ErKf5Y0.png',
						'https://www.npmjs.com/'
					)
					.setTitle(body.data.name)
					.setURL(`https://www.npmjs.com/package/${pkg}`)
					.setDescription(body.description || 'Sem descrição.')
					.addField('❯ Versão', body.data['dist-tags'].latest, true)
					.addField('❯ Licença', body.data.license || 'Não possui', true)
					.addField(
						'❯ Autor',
						body.data.author ? body.data.author.name : 'Sem Dono',
						true
					)
					.addField(
						'❯ Data de Criação',
						moment.utc(body.data.time.created).format('DD/MM/YYYY h:mm A'),
						true
					)
					.addField(
						'❯ Data de Modificação',
						moment.utc(body.data.time.modified).format('DD/MM/YYYY h:mm A'),
						true
					);

				return message.channel.send(embed);
			})
			.catch(err => {
				if (err.response.status === 404) {
					return message.channel.send(
						'Procurei em todo quando mas não achei esse npm...'
					);
				} else {
					message.channel.sensd(
						'Ocorreu um error ao tentar executar esse comando!'
					);
				}
			});
	}
};
