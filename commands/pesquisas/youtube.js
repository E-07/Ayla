const Discord = require('discord.js');
const { youtube } = require('ayla-npm');

module.exports = {
	name: 'youtube',
	aliases: ['yt'],
	category: 'pesquisas',
	args: 'text',
	run: (client, message, args) => {
		let yt = youtube({
			message: args.join(' ')
		});

		const embed = new Discord.MessageEmbed()
			.setColor('FF0000')
			.setTitle('<:youtube:763799991273259078> Youtube')
			.setDescription(`[Clique Aqui!](${yt})`)
			.setFooter(`Comando solicitado por: ${message.author.username}`)
			.setThumbnail('https://tenor.com/view/you-tube-logo-animation-transition-old-gif-14769705');

		message.channel.send(embed);
	}
};
