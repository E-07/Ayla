module.exports = {
	name: 'caps',
	args: 'text',
	category: 'texto',
	aliases: ['caps-lock', 'upcase'],
	run: (client, message, args) => {
		let text = args.join(' ');
		message.channel.send(text.toUpperCase());
	}
};
