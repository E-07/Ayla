const clientnk = require('nekos.life');
const neko = new clientnk();

module.exports = {
	name: 'catext',
	category: 'texto',
	aliases: ['cattext', 'textocat', 'gato', 'gatotexto'],
	run: (client, message, args) => {
		neko.sfw.catText().then(a => {
			message.channel.send(a.cat);
		});
	}
};
