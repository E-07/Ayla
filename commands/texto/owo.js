const clientnk = require('nekos.life');
const neko = new clientnk();

module.exports = {
	name: 'owo',
	category: 'texto',
	args: "text",
	aliases: ['owotext', 'textowo', 'owoify'],
	run: async (client, message, args) => {
		let owo = await neko.sfw.OwOify({ text: args.join(' ') });
		message.reply(owo.owo);
	}
};
