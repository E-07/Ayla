const clientnk = require('nekos.life');
const neko = new clientnk();

module.exports = {
	name: 'spoiler',
	category: 'texto',
	args: 'text',
	run: async (client, message, args) => {
		let spoiler = await neko.sfw.spoiler({ text: args.join(' ') });
		message.delete()
		message.channel.send(spoiler.owo);
	}
};
