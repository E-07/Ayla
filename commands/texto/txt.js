const Discord = require('discord.js');

module.exports = {
	name: 'txt',
	category: 'texto',
	run: (client, message, args) => {
		if (!args[0]) {
			return message.reply(
				'Escreva depois do comando o que vai ter dentro do arquivo txt!'
			);
		}
		message.channel.send({
			files: [{ attachment: Buffer.from(args.join(' ')), name: 'arquivo.txt' }]
		});
	}
};
