const Discord = require('discord.js');

module.exports = {
	name: 'avatar',
	category: 'utilidades',
	aliases: ['foto', 'ft'],
	run: (client, message, args) => {
		let user =
			message.mentions.users.first() ||
			client.users.cache.get(args[0]) ||
			client.users.cache.find(user => user.username === args[0]) ||
			message.author;
		let avatar = user.avatarURL({ dynamic: true, format: 'png', size: 1024 });

		let embed = new Discord.MessageEmbed()
			.setColor(`#fb00ff`)
			.setTitle(`Avatar do Usuário: ${user.username}`)
			.setImage(avatar);
		message.channel.send(embed);
	}
};
