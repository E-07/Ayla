const Discord = require('discord.js');
const { parse } = require('twemoji-parser');
const { MessageEmbed } = require('discord.js');

module.exports = {
	name: 'emoji',
	category: "utilidades",
	run: (client, message, args) => {
		const emoji = args[0];
		if (!emoji)
			return message.channel.send(
				`Por favor, coloque um emoji depois do comando!`
			);

		let customemoji = Discord.Util.parseEmoji(emoji);

		if (customemoji.id) {
			const Link = `https://cdn.discordapp.com/emojis/${customemoji.id}.${
				customemoji.animated ? 'gif' : 'png'
			}`;

			const Added = new MessageEmbed()

				.setColor(`fb00ff`)
				.addField('Link:', `[Clique em Mim](${Link})`)
				.setImage(Link);
			return message.channel.send(Added);
		} else {
			let CheckEmoji = parse(emoji, { assetType: 'png' });
			if (!CheckEmoji[0])
				return message.channel.send(`Por favor, coloque um emoji válido!`);
		}
	}
};
