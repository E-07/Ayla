const Discord = require('discord.js');
var imgur = require('imgur');
imgur.setAPIUrl('https://api.imgur.com/3/');
imgur.getAPIUrl();

module.exports = {
	name: 'imgur',
	aliases: ['link'],
	category: 'utilidades',
	run: (client, message, args) => {
		if (message.attachments.size < 1) {
			message.channel.send('Você deve mandar uma imagem!');
		} else {
			if (
				message.attachments.first().url.endsWith('png') ||
				message.attachments.first().url.endsWith('jpg') ||
				message.attachments.first().url.endsWith('gif')
			) {
				imgur
					.uploadUrl(message.attachments.first().url)
					.then(json => {
						message.channel.send('Link da imagem: ' + json.data.link);
					})
					.catch(err => {
						message.reply(`Aconteceu um error muito grave \`\`${err}\`\``);
					});
			} else {
				message.reply(
					'infelizmente, Só são suportados arquivos png, jpg e gif.'
				);
			}
		}
	}
};
