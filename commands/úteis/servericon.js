const Discord = require('discord.js');

module.exports = {
	name: 'servericon',
	aliases: ['icon', 'iconserver'],
	category: 'utilidades',
	run: (client, message, args) => {
		let guild =
			client.guilds.cache.get(args[0]) ||
			client.guilds.cache.find(guild => guild.name === args[0]) ||
			message.guild;

		let embed = new Discord.MessageEmbed()
			.setTitle(guild.name)
			.setColor('fb00ff')
			.setDescription(`[Clique Aqui!](${guild.iconURL()})`)
			.setImage(guild.iconURL());
		message.channel.send(embed);
	}
};
