const firebase = require('firebase');

let firebaseConfig = {
	apiKey: process.env.apikey,
	authDomain: process.env.authDomain,
	databaseURL: process.env.databaseURL,
	projectId: process.env.projectId,
	storageBucket: process.env.storageBucket,
	messagingSenderId: process.env.messagingSenderId,
	appId: process.env.appId,
	measurementId: process.env.measurementId
};

try {
	firebase.initializeApp(firebaseConfig);
} catch (e) {
	console.error(e);
}

const db = firebase.database();

module.exports = {
	db
};
