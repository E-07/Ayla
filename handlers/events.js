const { readdirSync } = require('fs');

module.exports = client => {
	const load = dirs => {
		const events = readdirSync(`./events/${dirs}/`).filter(d =>
			d.endsWith('.js')
		);

		for (let file of events) {
			const event = require(`../events/${dirs}/${file}`);
			let eventName = file.split('.')[0];
			client.on(eventName, event.bind(null, client));
		}
	};
	readdirSync(`./events/`).forEach(x => load(x));
};