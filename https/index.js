const express = require('express');
const app = express();

module.exports = {
	init: async function() {
		app.get('/', (req, res) => {
			res.sendStatus(200);
		});

		app.listen(8080, () => {
			console.log('Servidor rodando!');
		});
	}
};
