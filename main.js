/*
*       Ayla Bot
* Copyright (c) 2020 07
*/

const Discord = require('discord.js');
const client = new Discord.Client();
const servidor = require('./https/index.js');
const { db } = require('./firebase.js');
servidor.init();

client.commands = new Discord.Collection();
client.aliases = new Discord.Collection();
client.snipes = new Map();

try {
	client.login(process.env.TOKEN);
} catch (e) {
	console.error(e);
}

['command'].forEach(handler => {
	require(`./handlers/${handler}`)(client);
});

client.on('messageDelete', message => {
	if (message.author.bot) return;
	client.snipes.set(message.channel.id, {
		content: message.content,
		author: message.author.tag,
		image: message.attachments.first()
			? message.attachments.first().proxyURL
			: null
	});
});

client.on('message', message => {
	if (message.author.bot) return;
	if (message.channel.type === 'dm') return;

	db.ref(`servers/${message.guild.id}`)
		.once('value')
		.then(r => {
			if (r.val() === null) {
				db.ref(`servers/${message.guild.id}`).set({
					nome: message.guild.name,
					id: message.guild.id,
					prefixo: 'a!',
					dono: message.guild.ownerID,
					bot: false,
					invite: false
				});
			}
		});

	db.ref(`servers/${message.guild.id}`)
		.once('value')
		.then(r => {
			db.ref(`servers/${message.guild.id}`).update({
				nome: message.guild.name
			});
		});

	db.ref(`servers/${message.guild.id}`)
		.once('value')
		.then(a => {
			if (a.val() === null) {
				db.ref(`servers/${message.guild.id}`).set({
					nome: message.guild.name,
					id: message.guild.id,
					prefixo: 'a!',
					dono: message.guild.ownerID,
					bot: false,
					invite: false
				});
			}

			if (message.channel.topic == 'global on' && !message.author.bot) {
				let msg = message.content;

				const regex = /(https?:\/\/)?(www\.)?(discord\.(gg|io|me|li|club)|discordapp\.com\/invite|discord\.com\/invite)\/.+[a-z]/gi;
				if (regex.exec(message.content)) return;

				if (!message.content.toLowerCase().includes('http')) {
					if (message.content.length >= 200) return;
					client.guilds.cache.forEach(guild => {
						if (guild == message.guild) return;

						let chhannel = guild.channels.cache.find(
							ch => ch.topic === 'global on'
						);
						message.channel.setRateLimitPerUser(3);
						if (!chhannel) return;

						let embed = new Discord.MessageEmbed()
							.setAuthor(
								message.author.tag + ' | Mandou:',
								message.author.displayAvatarURL()
							)
							.setColor('#fb00ff')
							.setDescription(msg)
							.setFooter(
								'Servidor: ' + message.guild.name,
								message.guild.iconURL({ dynamic: true })
							)
							.setTimestamp();
						chhannel.send(embed).catch(e => {});
					});
				}
			}

			if (message.channel.id === '614563042809741388') {
				if (message.content.startsWith('>')) return;
				message.react('763566083881828393');
				message.react('766116333808123944');
			}

			db.ref(`servers/${message.guild.id}`)
				.once('value')
				.then(r => {
					if (r.val().invite === null || r.val().invite === undefined) {
						db.ref(`servers/${message.guild.id}`).update({
							invite: false
						});
					}

					let prefix = a.val().prefixo;

					if (
						message.content === `<@${client.user.id}>` ||
						message.content === `<@!${client.user.id}>` ||
						message.content === `<@&${client.user.id}>` ||
						message.content === '<@736654197176533173>'
					) {
						message.reply(
							`👋 Olá, ainda estou em beta, mas meu prefixo nesse servidor é \`${prefix}\` Utilize \`${prefix}ajuda\` Para saber mais sobre meus Comandos`
						);
					}

					if (message.webhookID) return;
					if (!message.content.startsWith(prefix)) return;

					const args = message.content
						.trim()
						.slice(prefix.length)
						.split(/ +/g);

					const cmd = args.shift().toLowerCase();

					if (cmd.length === 0) return;

					let command = client.commands.get(cmd);

					if (!command) command = client.commands.get(client.aliases.get(cmd));

					try {
						if (command.args) {
							if (command.args === 'text' && !args[0]) {
								return message.reply(
									'<:chekmarkno:766120433141153833> | Não é bem assim que usa esse comando mas tudo bem! tente colocar alguma coisa depois dele!'
								);
							}
						}
						if (
							command.dono === true &&
							message.author.id !== '532799065143115777' &&
							message.author.id !== '711969991427227729'
						) {
							return message.reply(
								'😅 | Desculpe, mas so o meu criador pode usar esse comando!'
							);
						}
						if (
							command.bug === true &&
							message.author.id !== '532799065143115777'
						) {
							return message.reply(
								'😶 | Eita! Infelizmente você não pode usar esse comando agora. Por favor, tente novamente mais tarde.'
							);
						}
					} catch (e) {
						console.error(e);
					}
					try {
						if (command) {
							command.run(client, message, args, db);
						}
					} catch (e) {
						console.error(e);
						message.reply(
							`😯 | Essa não! ocorreu um error extremamente grave ao tentar executar esse comando! Devido ao error: \`\`${e}\`\` Mas não se preocupe eu vou enviar isso direto para o meu suporte!`
						);
						let aylaGuild = client.guilds.cache.get('739290644928921740');

						let schannel = aylaGuild.channels.cache.get('740479730607718431');
						schannel.send(
							`😬 | Parece que aconteceu um bug no comando **${
								comand.name
							}** E o bug foi: \`\`${e}\`\``
						);
					}
				});
		});
});

client.on('ready', () => {
	let aylaGuild = client.guilds.cache.get('739290644928921740');

	let countServer = client.guilds.cache.size;

	let schannel = aylaGuild.channels.cache.get('740479730607718431');

	schannel.setName('📡・Servidores: ' + countServer);
	let countMembers = client.users.cache.size;

	let sschannel = aylaGuild.channels.cache.get('740479564878315560');

	sschannel.setName('💘・Usuários: ' + countMembers);

	let activities = [
			`💘 Protegendo ${client.guilds.cache.size} Servidores!`,
			`🍭 Cuidando de ${client.users.cache.size} Usuários!`,
			'👋 Olá, tudo bem? eu me chamo Ayla!'
		],
		i = 0;
	setInterval(
		() =>
			client.user.setActivity(`${activities[i++ % activities.length]}`, {
				type: 'WATCHING'
			}),
		5000
	);

	client.user.setStatus('online').catch(console.error);

	console.log('Fui Inciada com Sucesso!');
});

client.on('guildCreate', guild => {
	try {
		db.ref(`servers/${guild.id}`).set({
			nome: guild.name,
			id: guild.id,
			prefixo: 'a!',
			dono: guild.ownerID,
			bot: false,
			invite: false
		});
	} catch (e) {
		console.error(e);
	}

	let aylaGuild = client.guilds.cache.get('739290644928921740');
	let countServer = client.guilds.cache.size;
	let serverchannel = aylaGuild.channels.cache.get('740479730607718431');
	serverchannel.setName('📡・Servidores: ' + countServer);

	let countMembers = client.users.cache.size;
	let userschannel = aylaGuild.channels.cache.get('740479564878315560');
	userschannel.setName('💘・Usuários: ' + countMembers);

	let channel = aylaGuild.channels.cache.get('740600765239787661');

	if (!channel) return;

	let DonoServidor = client.users.cache.get(guild.ownerID);

	let embed = new Discord.MessageEmbed()
		.setThumbnail(guild.iconURL())
		.setTitle('<a:Yes:763566083881828393> Fui Adicionada em mais um Servidor!')
		.setDescription(
			`Aee! Mais um Servidor! agora eu estou em **${
				client.guilds.cache.size
			}** Servidores! 
O Servidor que eu foi adicionada tem **${
				guild.memberCount
			}** Membros! O Nome do servidor é **${guild.name}** E o Dono é o(a) **${
				DonoServidor.username
			}**
		
		
		`
		)
		.setColor('00FF1D');
	channel.send(embed);
});

client.on('guildDelete', guild => {
	try {
		db.ref(`servers/${guild.id}`).remove();
	} catch (e) {
		console.error(e);
	}
	let aylaGuild = client.guilds.cache.get('739290644928921740');
	let countServer = client.guilds.cache.size;
	let schannel = aylaGuild.channels.cache.get('740479730607718431');
	schannel.setName('📡・Servidores: ' + countServer);

	let countMembers = client.users.cache.size;
	let sschannel = aylaGuild.channels.cache.get('740479564878315560');
	sschannel.setName('💘・Usuários: ' + countMembers);

	let channel = aylaGuild.channels.cache.get('740600765239787661');

	if (!channel) return;

	let dono = client.users.cache.get(guild.ownerID);

	let embed = new Discord.MessageEmbed()
		.setThumbnail(guild.iconURL())
		.setTitle('<a:sadd:766116333808123944> Menos um Servidor! 😥')
		.setDescription(
			`Fui removida do Servidor **${guild.name}** E o(a) dono(a) é o(a) **${
				dono.username
			}** Isso e meio triste 😥 Mas tudo bem! Eu ainda estou em **${
				clieServclientcache.size
			}** Servidores!
		
		`
		)
		.setColor('FF0000');
	channel.send(embed);
});

client.on('guildMemberAdd', member => {
	let aylaGuild = client.guilds.cache.get('739290644928921740');

	let countServer = client.guilds.cache.size;

	let schannel = aylaGuild.channels.cache.get('740479730607718431');

	schannel.setName('📡・Servidores: ' + countServer);

	let countMembers = client.users.cache.size;

	let sschannel = aylaGuild.channels.cache.get('740479564878315560');

	sschannel.setName('💘・Usuários: ' + countMembers);

	db.ref(`servers/${member.guild.id}`)
		.once('value')
		.then(r => {
			if (r.val() === null) return;
			if (member.user.bot === true && r.val().bot === true) {
				console.log(`${member.user.username} kitado!`);
				member.kick(['Anti bot Ativado!']);
			}
		});
});

client.on('guildMemberRemove', member => {
	let aylaGuild = client.guilds.cache.get('739290644928921740');
	let countServer = client.guilds.cache.size;
	let schannel = aylaGuild.channels.cache.get('740479730607718431');
	schannel.setName('📡・Servidores: ' + countServer);

	let countMembers = client.users.cache.size;
	let sschannel = aylaGuild.channels.cache.get('740479564878315560');
	sschannel.setName('💘・Usuários: ' + countMembers);
});