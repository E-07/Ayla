const { ShardingManager } = require('discord.js');

const manager = new ShardingManager('./main.js', {
	token: process.env.TOKEN,
	totalShards: parseInt(process.env.shards) || 'auto'
});

manager.spawn();
manager.on('shardCreate', shard => console.log(`Lançando shard: ${shard.id}`));